class UserModel{

  int id;
  String username;
  String password;
  String token;


  UserModel.fromJson(Map<String, dynamic> json) :

      id = json['id'],
      username = json['username'],
      password = json['password'],
      token = json['token'];

}