import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:fast/localization/localization.dart';

import '../utils/ui_elements.dart';
import '../resources/firebase_authentication.dart';

class AuthenticationPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => AuthenticationPageState();
}

class AuthenticationPageState extends State<AuthenticationPage>
    with WidgetsBindingObserver {
  CustomTextField _textFieldEmail;
  CustomTextField _textFieldPassword;

  @override
  void initState() {
    _textFieldEmail = CustomTextField().email();
    _textFieldPassword = CustomTextField().password();

    WidgetsBinding.instance.addObserver(this);
    WidgetsBinding.instance.addPostFrameCallback(addPostFrameCallback);

    super.initState();
  }

  @override
  void dispose() {
    disposeAll();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle.light
            .copyWith(statusBarColor: Colors.transparent),
        sized: false,
        child: Scaffold(
            body: GestureDetector(
                onTap: () =>
                    FocusScope.of(context).requestFocus(new FocusNode()),
                child: LinearGradientBackground(_buildColumn()))));
  }

  Widget _buildColumn() {
    Locale locale = Localizations.localeOf(context);

    return SafeArea(
        child: Center(
            child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
          Column(
            children: <Widget>[
              _buildTextFieldContainer(_textFieldEmail),
              _buildTextFieldContainer(_textFieldPassword),
            ],
          ),
          PrimaryButton(AppLocalization(locale).signIn, validate),
        ])));
  }

  Widget _buildTextFieldContainer(CustomTextField customTextField) {
    MediaQueryData mediaQueryData = MediaQuery.of(context);
    double padding = mediaQueryData.padding.top;

    return Container(
        margin:
            EdgeInsets.only(left: padding, right: padding, top: padding / 2),
        padding: EdgeInsets.only(left: padding, right: padding),
        width: mediaQueryData.size.width,
        decoration: _boxDecoration(),
        child: _buildTextField(customTextField));
  }

  BoxDecoration _boxDecoration() {
    return BoxDecoration(
        color: Colors.white.withOpacity(0.1),
        borderRadius: BorderRadius.circular(10));
  }

  Widget _buildTextField(CustomTextField customTextField) {
    return TextField(
      keyboardType: customTextField.textInputType,
      obscureText: customTextField.obscure ?? false,
      focusNode: customTextField?.focusNode,
      controller: customTextField?.textEditingController,
      style: _textStyle(customTextField),
      decoration: _inputDecoration(customTextField),
    );
  }

  InputDecoration _inputDecoration(CustomTextField customTextField) {
    return InputDecoration(
        icon: customTextField.icon,
        enabledBorder: underlineInputBorder(),
        focusedBorder: underlineInputBorder(),

        labelText: customTextField.label,
        labelStyle: _textStyle(customTextField),
        errorText:
            customTextField.validated ? null : customTextField.errorText);
  }

  UnderlineInputBorder underlineInputBorder() {
    return UnderlineInputBorder(
        borderSide: BorderSide(color: Colors.transparent));
  }

  TextStyle _textStyle(CustomTextField customTextField) {
    MediaQueryData mediaQueryData = MediaQuery.of(context);
    return TextStyle(
        color:
            customTextField.focusNode.hasFocus ? Colors.grey : Colors.black45,
        fontSize: 14 * mediaQueryData.textScaleFactor);
  }

  void addPostFrameCallback(Duration duration) {
    _textFieldEmail.focusNode.addListener(() => setState(() {}));
    _textFieldPassword.focusNode.addListener(() => setState(() {}));

    Locale locale = Localizations.localeOf(context);

    _textFieldEmail.label = AppLocalization(locale).email;
    _textFieldPassword.label = AppLocalization(locale).password;

    _textFieldEmail.errorText = AppLocalization(locale).emailError;
    _textFieldPassword.errorText = AppLocalization(locale).passwordError;

    setState(() {});
  }

  void disposeAll() {
    _textFieldEmail.focusNode.dispose();
    _textFieldPassword.focusNode.dispose();
    _textFieldEmail.textEditingController.dispose();
    _textFieldPassword.textEditingController.dispose();
  }

  void validate() {
    String emailText = _textFieldEmail.textEditingController.text;
    String passwordText = _textFieldPassword.textEditingController.text;

    bool emailValidated = _textFieldEmail.validateEmail(emailText);
    bool passwordValidated = _textFieldPassword.validatePassword(passwordText);

    if(emailValidated && passwordValidated)
    FirebaseAuthentication().signInWithEmailAndPassword(emailText, passwordText);


    setState(() {});
  }
}

class CustomTextField {
  BuildContext context;
  String label;
  Icon icon;
  FocusNode focusNode = FocusNode();
  bool obscure = false;
  TextInputType textInputType;
  TextEditingController textEditingController = TextEditingController();
  String errorText;
  bool validated = true;

  CustomTextField(
      {this.label,
      this.icon,
      this.obscure,
      this.textInputType,
      this.errorText,
      this.validated});

  CustomTextField email() {
    return CustomTextField(
        label: label,
        icon: Icon(Icons.email),
        textInputType: TextInputType.emailAddress,
        validated: true,
        errorText: errorText);
  }

  CustomTextField password() {
    return CustomTextField(
        label: label,
        icon: Icon(Icons.lock),
        obscure: true,
        validated: true,
        errorText: errorText);
  }

  bool validateEmail(String value) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
    if (!regex.hasMatch(value)) {
      validated = false;
    } else {
      validated = true;
    }
    return validated;
  }

  bool validatePassword(String value) {
    if (value.length < 8) {
      validated = false;
    } else {
      validated = true;
    }
    return validated;
  }
}
