import 'package:flutter/material.dart';

class LinearGradientBackground extends StatelessWidget {
  final Widget child;

  LinearGradientBackground(this.child);

  @override
  Widget build(BuildContext context) {
    return linearGradientBackground();
  }

  Widget linearGradientBackground() {
    return Container(decoration: boxDecoration(), child: child);
  }

  BoxDecoration boxDecoration() {
    return BoxDecoration(
        gradient: LinearGradient(
      begin: Alignment.topCenter,
      end: Alignment.bottomCenter,
      stops: [0, 0.5, 1],
      colors: [
        Color(0xff415B72),
        Color(0xff2C4962),
        Color(0xff173753),
      ],
    ));
  }
}

class PrimaryButton extends StatelessWidget {
  final String title;
  final Function function;

  PrimaryButton(this.title, this.function);

  @override
  Widget build(BuildContext context) {
    return _buildContainer(context);
  }

  Widget _buildContainer(BuildContext context) {

    MediaQueryData mediaQueryData = MediaQuery.of(context);
    double padding = mediaQueryData.padding.top;

    return Padding(
        padding: EdgeInsets.all(10),
        child:  Container(
        margin: EdgeInsets.only(left: padding, right: padding, top: padding / 2),
        padding: EdgeInsets.only(left: padding, right: padding),
        width: mediaQueryData.size.width,
        decoration: _boxDecoration(),
        child: _buildButton()));
  }


  BoxDecoration _boxDecoration() {
    return BoxDecoration(
        color: Colors.white.withOpacity(0.1),
        borderRadius: BorderRadius.circular(10));
  }

  Widget _buildButton() {
    return FlatButton(
        onPressed: function,
        child: Text(title));
  }
}
