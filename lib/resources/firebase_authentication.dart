import 'package:firebase_auth/firebase_auth.dart';


class FirebaseAuthentication{

  final FirebaseAuth _auth = FirebaseAuth.instance;


  void signInWithEmailAndPassword(String email, String password) async{
    try{
    final AuthResult result = (await _auth.signInWithEmailAndPassword
      (email: email, password: password));

    }catch(PlatformException){
      print(PlatformException);
    }


  }

}