import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

import './pages/authentication_page.dart';
import './pages/home_page.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'FAST',
      theme: ThemeData(
        primarySwatch: Colors.grey,
      ),
        localizationsDelegates: [
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
          GlobalCupertinoLocalizations.delegate,
        ],
        supportedLocales: [
          const Locale('en'), // English
          const Locale('es'), // Spanish
        ],
      routes: {
        '/': (BuildContext context) => AuthenticationPage(),
        '/home': (BuildContext context) => HomePage(),
      },
      onUnknownRoute: (settings){
        return MaterialPageRoute(builder: (context){
          return AuthenticationPage();
        });
      }
    );
  }
}
