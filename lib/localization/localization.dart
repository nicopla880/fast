import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart' show SynchronousFuture;

class AppLocalization {
  AppLocalization(this.locale);

  final Locale locale;

  static AppLocalization of(BuildContext context) {
    return Localizations.of<AppLocalization>(context, AppLocalization);
  }

  static Map<String, Map<String, String>> _localizedValues = {
    'en': {
      'title': 'FAST',

      'email': 'Email',
      'password': 'Password',

      'validate_email': 'Enter a valid email address',
      'validate_password': 'Enter a valid password',

      'authentication_login': 'Sign in',

    },
    'es': {
      'title': 'FAST',

      'email': 'Email',
      'password': 'Contraseña',

      'validate_email': 'Ingrese un email válido',
      'validate_password': 'Ingrese una contraseña con mas de 8 caracteres',

      'authentication_login': 'Iniciar sesión',

    },
  };

  String get title {
    return _localizedValues[locale.languageCode]['title'];
  }
  String get signIn {
    return _localizedValues[locale.languageCode]['authentication_login'];
  }
  String get email {
    return _localizedValues[locale.languageCode]['email'];
  }
  String get password {
    return _localizedValues[locale.languageCode]['password'];
  }

  String get emailError {
    return _localizedValues[locale.languageCode]['validate_email'];
  }
  String get passwordError {
    return _localizedValues[locale.languageCode]['validate_password'];
  }
}

class DemoLocalizationsDelegate extends LocalizationsDelegate<AppLocalization> {
  const DemoLocalizationsDelegate();

  @override
  bool isSupported(Locale locale) => ['en', 'es'].contains(locale.languageCode);

  @override
  Future<AppLocalization> load(Locale locale) {

    return SynchronousFuture<AppLocalization>(AppLocalization(locale));
  }

  @override
  bool shouldReload(DemoLocalizationsDelegate old) => false;
}